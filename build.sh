#!/bin/bash
SDK_PATH=/src/orangepi_h2_linux
KERNEL_PATH=$SDK_PATH/OrangePi-Kernel
CUSTOM_KERNEL_PATH=custom/OrangePi-Kernel

#prepare custom script.bin from script.fex from our repo
rm $SDK_PATH/OrangePi-BuildLinux/orange/script.bin
fex2bin custom/script.fex $SDK_PATH/OrangePi-BuildLinux/orange/script.bin

#prepare custom configuration
cp -r $CUSTOM_KERNEL_PATH/chips/sun8iw7p1/bin/* $KERNEL_PATH/chips/sun8iw7p1/bin
cp -r $CUSTOM_KERNEL_PATH/chips/sun8iw7p1/configs/* $KERNEL_PATH/chips/sun8iw7p1/configs
cp -r $CUSTOM_KERNEL_PATH/linux-3.4/arch/arm/configs/sun8iw7p1smp_linux_defconfig $KERNEL_PATH/build/sun8iw7p1smp_android_defconfig

cd $SDK_PATH

#install bootloader and kernel
cd $KERNEL_PATH

./build_uboot.sh zero
./build_linux_kernel.sh zero

#create image
cd ../OrangePi-BuildLinux
./create_image
./image_from_dir linux-xenial orangepi ext4 zero

#post-build
cp /src/orangepi_h2_linux/OrangePi-BuildLinux/orangepi.img /builds/dronov/pizero-docker/builds
