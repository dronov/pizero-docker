### Orange Pi Zero SDK in Docker
[![build status](https://gitlab.com/dronov/pizero-docker/badges/master/build.svg)](https://gitlab.com/dronov/pizero-docker/commits/master)

### What is it

This is a Dockerfile describing build instructions for Orange Pi Zero SDK. It downloads SDK from [Xunlong H2 Linux SDK Git repository](https://github.com/orangepi-xunlong/orangepi_h2_linux) and prepares development environment.

### How to build

* Install Docker
* Clone this repo
* Build containter by ```docker build -t pizero .```
* Run container with privileged access ```docker run --privileged --rm -ti -v /home:/share pizero /bin/bash```
* ```cd /src/orangepi_h2_linux``` and read README to build image with Linux

### Example

You could watch a build.sh script for example how to build your image.
