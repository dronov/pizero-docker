FROM ubuntu:xenial

RUN mkdir /src
WORKDIR /src

RUN apt-get -y update && apt-get -y upgrade && \
    apt-get -y install build-essential device-tree-compiler lib32stdc++6\
    libusb-1.0-0 libusb-1.0-0-dev lib32z1 lib32ncurses5 u-boot-tools \
    git wget fakeroot kernel-package zlib1g-dev libncurses5-dev vim \
    debootstrap qemu-user-static parted udev dosfstools

RUN git clone https://github.com/orangepi-xunlong/orangepi_h2_linux.git
ENV PATH=$PATH:/src/orangepi_h2_linux/OrangePi-Kernel/brandy/gcc-linaro/bin/

RUN git clone git://github.com/linux-sunxi/sunxi-tools.git
WORKDIR /src/sunxi-tools
RUN make fex2bin
RUN chmod +x fex2bin
RUN cp fex2bin /usr/local/bin/

CMD /bin/sh
